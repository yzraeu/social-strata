﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SocialStrata.Models;

namespace SocialStrata.Controllers.API
{
	public class BuildingsController : ApiController
	{
		private ModelContext db = new ModelContext();

		// GET: api/Buildings
		public IQueryable<Building> GetBuildings()
		{
			return db.Buildings
				.Include(b => b.Amenities)
				.Include(b => b.Towers)
				.Include(b => b.Address);
		}

		// GET: api/Buildings/5
		[ResponseType(typeof(Building))]
		public async Task<IHttpActionResult> GetBuilding(Guid id)
		{
			Building building = await db.Buildings
				.Include(b => b.Amenities)
				.Include(b => b.Towers)
				.Include(b => b.Address)
				.FirstOrDefaultAsync(b => b.Id.Equals(id));

			if (building == null)
			{
				return NotFound();
			}

			return Ok(building);
		}

		// PUT: api/Buildings/5
		[ResponseType(typeof(void))]
		public async Task<IHttpActionResult> PutBuilding(Guid id, Building building)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != building.Id)
			{
				return BadRequest();
			}

			db.Entry(building).State = EntityState.Modified;

			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!BuildingExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return StatusCode(HttpStatusCode.NoContent);
		}

		// POST: api/Buildings
		[ResponseType(typeof(Building))]
		public async Task<IHttpActionResult> PostBuilding(Building building)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			building.Id = Guid.NewGuid();
			db.Buildings.Add(building);

			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateException)
			{
				if (BuildingExists(building.Id))
				{
					return Conflict();
				}
				else
				{
					throw;
				}
			}

			return CreatedAtRoute("DefaultApi", new { id = building.Id }, building);
		}

		// DELETE: api/Buildings/5
		[ResponseType(typeof(Building))]
		public async Task<IHttpActionResult> DeleteBuilding(Guid id)
		{
			Building building = await db.Buildings.FindAsync(id);
			if (building == null)
			{
				return NotFound();
			}

			db.Buildings.Remove(building);
			await db.SaveChangesAsync();

			return Ok(building);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		private bool BuildingExists(Guid id)
		{
			return db.Buildings.Count(e => e.Id == id) > 0;
		}
	}
}