﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SocialStrata.Models;

namespace SocialStrata.Controllers.API
{
	public class LostAndFoundController : ApiController
	{
		private ModelContext db = new ModelContext();

		// GET: api/LostAndFound
		public IQueryable<LostAndFound> GetLostAndFound()
		{
			return db.LostAndFound
				.Include(lf => lf.Building)
				.Include(lf => lf.User);
		}

		// GET: api/LostAndFound/5
		[ResponseType(typeof(LostAndFound))]
		public async Task<IHttpActionResult> GetLostAndFound(Guid id)
		{
			LostAndFound lostAndFound = await db.LostAndFound
				.Include(lf => lf.Building)
				.Include(lf => lf.User)
				.FirstOrDefaultAsync(lf => lf.Id.Equals(id));

			if (lostAndFound == null)
			{
				return NotFound();
			}

			return Ok(lostAndFound);
		}

		// PUT: api/LostAndFound/5
		[ResponseType(typeof(void))]
		public async Task<IHttpActionResult> PutLostAndFound(Guid id, LostAndFound lostAndFound)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != lostAndFound.Id)
			{
				return BadRequest();
			}

			db.Entry(lostAndFound).State = EntityState.Modified;

			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!LostAndFoundExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return StatusCode(HttpStatusCode.NoContent);
		}

		// POST: api/LostAndFound
		[ResponseType(typeof(LostAndFound))]
		public async Task<IHttpActionResult> PostLostAndFound(LostAndFound lostAndFound)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			lostAndFound.Id = Guid.NewGuid();
			db.LostAndFound.Add(lostAndFound);

			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateException)
			{
				if (LostAndFoundExists(lostAndFound.Id))
				{
					return Conflict();
				}
				else
				{
					throw;
				}
			}

			return CreatedAtRoute("DefaultApi", new { id = lostAndFound.Id }, lostAndFound);
		}

		// DELETE: api/LostAndFound/5
		[ResponseType(typeof(LostAndFound))]
		public async Task<IHttpActionResult> DeleteLostAndFound(Guid id)
		{
			LostAndFound lostAndFound = await db.LostAndFound.FindAsync(id);
			if (lostAndFound == null)
			{
				return NotFound();
			}

			db.LostAndFound.Remove(lostAndFound);
			await db.SaveChangesAsync();

			return Ok(lostAndFound);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		private bool LostAndFoundExists(Guid id)
		{
			return db.LostAndFound.Count(e => e.Id == id) > 0;
		}
	}
}