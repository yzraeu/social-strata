﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SocialStrata.Models;

namespace SocialStrata.Controllers.API
{
    public class MaintenanceRequestsController : ApiController
    {
        private ModelContext db = new ModelContext();

        // GET: api/MaintenanceRequests
        public IQueryable<MaintenanceRequest> GetMaintenanceRequests()
        {
            return db.MaintenanceRequests
				.Include(mr => mr.Building)
				.Include(mr => mr.User);
		}

        // GET: api/MaintenanceRequests/5
        [ResponseType(typeof(MaintenanceRequest))]
        public async Task<IHttpActionResult> GetMaintenanceRequest(Guid id)
        {
            MaintenanceRequest maintenanceRequest = await db.MaintenanceRequests
				.Include(mr => mr.Building)
				.Include(mr => mr.User)
				.FirstOrDefaultAsync(mr => mr.Id.Equals(id));

            if (maintenanceRequest == null)
            {
                return NotFound();
            }

            return Ok(maintenanceRequest);
        }

        // PUT: api/MaintenanceRequests/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMaintenanceRequest(Guid id, MaintenanceRequest maintenanceRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != maintenanceRequest.Id)
            {
                return BadRequest();
            }

            db.Entry(maintenanceRequest).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaintenanceRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MaintenanceRequests
        [ResponseType(typeof(MaintenanceRequest))]
        public async Task<IHttpActionResult> PostMaintenanceRequest(MaintenanceRequest maintenanceRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
			maintenanceRequest.Id = Guid.NewGuid();
			db.MaintenanceRequests.Add(maintenanceRequest);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MaintenanceRequestExists(maintenanceRequest.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = maintenanceRequest.Id }, maintenanceRequest);
        }

        // DELETE: api/MaintenanceRequests/5
        [ResponseType(typeof(MaintenanceRequest))]
        public async Task<IHttpActionResult> DeleteMaintenanceRequest(Guid id)
        {
            MaintenanceRequest maintenanceRequest = await db.MaintenanceRequests.FindAsync(id);
            if (maintenanceRequest == null)
            {
                return NotFound();
            }

            db.MaintenanceRequests.Remove(maintenanceRequest);
            await db.SaveChangesAsync();

            return Ok(maintenanceRequest);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MaintenanceRequestExists(Guid id)
        {
            return db.MaintenanceRequests.Count(e => e.Id == id) > 0;
        }
    }
}