﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SocialStrata.Models;

namespace SocialStrata.Controllers.API
{
    public class NoticeBoardController : ApiController
    {
        private ModelContext db = new ModelContext();

        // GET: api/NoticeBoard
        public IQueryable<NoticeBoard> GetNoticeBoard()
        {
            return db.NoticeBoard
				.Include(nb => nb.Building)
				.Include(nb => nb.User);
		}

        // GET: api/NoticeBoard/5
        [ResponseType(typeof(NoticeBoard))]
        public async Task<IHttpActionResult> GetNoticeBoard(Guid id)
        {
            NoticeBoard noticeBoard = await db.NoticeBoard
				.Include(nb => nb.Building)
				.Include(nb => nb.User)
				.FirstOrDefaultAsync(nb => nb.Id.Equals(id));

			if (noticeBoard == null)
            {
                return NotFound();
            }

            return Ok(noticeBoard);
        }

        // PUT: api/NoticeBoard/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutNoticeBoard(Guid id, NoticeBoard noticeBoard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != noticeBoard.Id)
            {
                return BadRequest();
            }

            db.Entry(noticeBoard).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NoticeBoardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/NoticeBoard
        [ResponseType(typeof(NoticeBoard))]
        public async Task<IHttpActionResult> PostNoticeBoard(NoticeBoard noticeBoard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
			noticeBoard.Id = Guid.NewGuid();
			db.NoticeBoard.Add(noticeBoard);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (NoticeBoardExists(noticeBoard.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = noticeBoard.Id }, noticeBoard);
        }

        // DELETE: api/NoticeBoard/5
        [ResponseType(typeof(NoticeBoard))]
        public async Task<IHttpActionResult> DeleteNoticeBoard(Guid id)
        {
            NoticeBoard noticeBoard = await db.NoticeBoard.FindAsync(id);
            if (noticeBoard == null)
            {
                return NotFound();
            }

            db.NoticeBoard.Remove(noticeBoard);
            await db.SaveChangesAsync();

            return Ok(noticeBoard);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NoticeBoardExists(Guid id)
        {
            return db.NoticeBoard.Count(e => e.Id == id) > 0;
        }
    }
}