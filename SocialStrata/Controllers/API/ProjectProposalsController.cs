﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SocialStrata.Models;

namespace SocialStrata.Controllers.API
{
    public class ProjectProposalsController : ApiController
    {
        private ModelContext db = new ModelContext();

        // GET: api/ProjectProposals
        public IQueryable<ProjectProposal> GetProjectProposals()
        {
	        return db.ProjectProposals
		        .Include(pp => pp.Building)
		        .Include(pp => pp.User)
		        .Include(pp => pp.Votes);
        }

        // GET: api/ProjectProposals/5
        [ResponseType(typeof(ProjectProposal))]
        public async Task<IHttpActionResult> GetProjectProposal(Guid id)
        {
            ProjectProposal projectProposal = await db.ProjectProposals
				.Include(pp => pp.Building)
				.Include(pp => pp.User)
				.Include(pp => pp.Votes)
				.FirstOrDefaultAsync(pp => pp.Id.Equals(id));

			if (projectProposal == null)
            {
                return NotFound();
            }

            return Ok(projectProposal);
        }

        // PUT: api/ProjectProposals/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProjectProposal(Guid id, ProjectProposal projectProposal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != projectProposal.Id)
            {
                return BadRequest();
            }

            db.Entry(projectProposal).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProjectProposalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProjectProposals
        [ResponseType(typeof(ProjectProposal))]
        public async Task<IHttpActionResult> PostProjectProposal(ProjectProposal projectProposal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
			projectProposal.Id = Guid.NewGuid();
			db.ProjectProposals.Add(projectProposal);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProjectProposalExists(projectProposal.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = projectProposal.Id }, projectProposal);
        }

		[ResponseType(typeof(ProjectProposalVote))]
		public async Task<IHttpActionResult> PostProjectProposalVote(ProjectProposalVote projectProposalVote)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			projectProposalVote.Id = Guid.NewGuid();
			db.ProjectProposalVotes.Add(projectProposalVote);

			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateException)
			{
				if (ProjectProposalVoteExists(projectProposalVote.Id))
				{
					return Conflict();
				}
				else
				{
					throw;
				}
			}

			return CreatedAtRoute("DefaultApi", new { id = projectProposalVote.Id }, projectProposalVote);
		}

		// DELETE: api/ProjectProposals/5
		[ResponseType(typeof(ProjectProposal))]
        public async Task<IHttpActionResult> DeleteProjectProposal(Guid id)
        {
            ProjectProposal projectProposal = await db.ProjectProposals.FindAsync(id);
            if (projectProposal == null)
            {
                return NotFound();
            }

            db.ProjectProposals.Remove(projectProposal);
            await db.SaveChangesAsync();

            return Ok(projectProposal);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProjectProposalExists(Guid id)
        {
            return db.ProjectProposals.Count(e => e.Id == id) > 0;
        }

		private bool ProjectProposalVoteExists(Guid id)
		{
			return db.ProjectProposalVotes.Count(e => e.Id == id) > 0;
		}
	}
}