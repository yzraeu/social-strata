﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SocialStrata.Models;

namespace SocialStrata.Controllers.API
{
	public class PublicSpaceBookingController : ApiController
	{
		private ModelContext db = new ModelContext();

		// GET: api/GetSchedule
		public IQueryable<AmenitySchedule> GetSchedule()
		{
			return db.AmenitySchedules
				.Include(s => s.Building)
				.Include(s => s.Amenity)
				.Include(s => s.User);

		}

		// GET: api/GetReservation/5
		[ResponseType(typeof(AmenitySchedule))]
		public async Task<IHttpActionResult> GetReservation(Guid id)
		{
			AmenitySchedule amenitySchedule = await db.AmenitySchedules
				.Include(s => s.Building)
				.Include(s => s.Amenity)
				.Include(s => s.User)
				.FirstOrDefaultAsync(s => s.Id.Equals(id));

			if (amenitySchedule == null)
			{
				return NotFound();
			}

			return Ok(amenitySchedule);
		}

		// PUT: api/PutReservation/5
		[ResponseType(typeof(void))]
		public async Task<IHttpActionResult> PutReservation(Guid id, AmenitySchedule amenitySchedule)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != amenitySchedule.Id)
			{
				return BadRequest();
			}

			db.Entry(amenitySchedule).State = EntityState.Modified;

			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!AmenityScheduleExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return StatusCode(HttpStatusCode.NoContent);
		}

		// POST: api/PostReservation
		[ResponseType(typeof(AmenitySchedule))]
		public async Task<IHttpActionResult> PostReservation(AmenitySchedule amenitySchedule)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			amenitySchedule.Id = Guid.NewGuid();
			db.AmenitySchedules.Add(amenitySchedule);

			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateException)
			{
				if (AmenityScheduleExists(amenitySchedule.Id))
				{
					return Conflict();
				}
				else
				{
					throw;
				}
			}

			return CreatedAtRoute("DefaultApi", new { id = amenitySchedule.Id }, amenitySchedule);
		}

		// DELETE: api/DeleteReservation/5
		[ResponseType(typeof(AmenitySchedule))]
		public async Task<IHttpActionResult> DeleteReservation(Guid id)
		{
			AmenitySchedule amenitySchedule = await db.AmenitySchedules.FindAsync(id);
			if (amenitySchedule == null)
			{
				return NotFound();
			}

			db.AmenitySchedules.Remove(amenitySchedule);
			await db.SaveChangesAsync();

			return Ok(amenitySchedule);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		private bool AmenityScheduleExists(Guid id)
		{
			return db.AmenitySchedules.Count(e => e.Id == id) > 0;
		}
	}
}