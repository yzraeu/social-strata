﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SocialStrata.Models;

namespace SocialStrata.Controllers
{
    public class AmenitySchedulesController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: AmenitySchedules
        public async Task<ActionResult> Index()
        {
            return View(await db.AmenitySchedules.ToListAsync());
        }

        // GET: AmenitySchedules/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmenitySchedule amenitySchedule = await db.AmenitySchedules.FindAsync(id);
            if (amenitySchedule == null)
            {
                return HttpNotFound();
            }
            return View(amenitySchedule);
        }

        // GET: AmenitySchedules/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AmenitySchedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StartTime,EndTime,CreatedAt,UpdatedAt")] AmenitySchedule amenitySchedule)
        {
            if (ModelState.IsValid)
            {
                amenitySchedule.Id = Guid.NewGuid();
                db.AmenitySchedules.Add(amenitySchedule);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(amenitySchedule);
        }

        // GET: AmenitySchedules/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmenitySchedule amenitySchedule = await db.AmenitySchedules.FindAsync(id);
            if (amenitySchedule == null)
            {
                return HttpNotFound();
            }
            return View(amenitySchedule);
        }

        // POST: AmenitySchedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StartTime,EndTime,CreatedAt,UpdatedAt")] AmenitySchedule amenitySchedule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(amenitySchedule).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(amenitySchedule);
        }

        // GET: AmenitySchedules/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmenitySchedule amenitySchedule = await db.AmenitySchedules.FindAsync(id);
            if (amenitySchedule == null)
            {
                return HttpNotFound();
            }
            return View(amenitySchedule);
        }

        // POST: AmenitySchedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            AmenitySchedule amenitySchedule = await db.AmenitySchedules.FindAsync(id);
            db.AmenitySchedules.Remove(amenitySchedule);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
