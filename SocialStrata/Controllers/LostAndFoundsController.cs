﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SocialStrata.Models;

namespace SocialStrata.Controllers
{
    public class LostAndFoundsController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: LostAndFounds
        public async Task<ActionResult> Index()
        {
            return View(await db.LostAndFound.ToListAsync());
        }

        // GET: LostAndFounds/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LostAndFound lostAndFound = await db.LostAndFound.FindAsync(id);
            if (lostAndFound == null)
            {
                return HttpNotFound();
            }
            return View(lostAndFound);
        }

        // GET: LostAndFounds/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LostAndFounds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Photo,Status,CreatedAt,UpdatedAt")] LostAndFound lostAndFound)
        {
            if (ModelState.IsValid)
            {
                lostAndFound.Id = Guid.NewGuid();
                db.LostAndFound.Add(lostAndFound);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(lostAndFound);
        }

        // GET: LostAndFounds/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LostAndFound lostAndFound = await db.LostAndFound.FindAsync(id);
            if (lostAndFound == null)
            {
                return HttpNotFound();
            }
            return View(lostAndFound);
        }

        // POST: LostAndFounds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Photo,Status,CreatedAt,UpdatedAt")] LostAndFound lostAndFound)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lostAndFound).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(lostAndFound);
        }

        // GET: LostAndFounds/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LostAndFound lostAndFound = await db.LostAndFound.FindAsync(id);
            if (lostAndFound == null)
            {
                return HttpNotFound();
            }
            return View(lostAndFound);
        }

        // POST: LostAndFounds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            LostAndFound lostAndFound = await db.LostAndFound.FindAsync(id);
            db.LostAndFound.Remove(lostAndFound);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
