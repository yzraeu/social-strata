﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SocialStrata.Models;

namespace SocialStrata.Controllers
{
    public class NoticeBoardsController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: NoticeBoards
        public async Task<ActionResult> Index()
        {
            return View(await db.NoticeBoard.ToListAsync());
        }

        // GET: NoticeBoards/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NoticeBoard noticeBoard = await db.NoticeBoard.FindAsync(id);
            if (noticeBoard == null)
            {
                return HttpNotFound();
            }
            return View(noticeBoard);
        }

        // GET: NoticeBoards/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NoticeBoards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Body,CreatedAt,UpdatedAt")] NoticeBoard noticeBoard)
        {
            if (ModelState.IsValid)
            {
                noticeBoard.Id = Guid.NewGuid();
                db.NoticeBoard.Add(noticeBoard);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(noticeBoard);
        }

        // GET: NoticeBoards/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NoticeBoard noticeBoard = await db.NoticeBoard.FindAsync(id);
            if (noticeBoard == null)
            {
                return HttpNotFound();
            }
            return View(noticeBoard);
        }

        // POST: NoticeBoards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Body,CreatedAt,UpdatedAt")] NoticeBoard noticeBoard)
        {
            if (ModelState.IsValid)
            {
                db.Entry(noticeBoard).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(noticeBoard);
        }

        // GET: NoticeBoards/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NoticeBoard noticeBoard = await db.NoticeBoard.FindAsync(id);
            if (noticeBoard == null)
            {
                return HttpNotFound();
            }
            return View(noticeBoard);
        }

        // POST: NoticeBoards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            NoticeBoard noticeBoard = await db.NoticeBoard.FindAsync(id);
            db.NoticeBoard.Remove(noticeBoard);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
