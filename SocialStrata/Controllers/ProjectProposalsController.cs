﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SocialStrata.Models;

namespace SocialStrata.Controllers
{
    public class ProjectProposalsController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: ProjectProposals
        public async Task<ActionResult> Index()
        {
            return View(await db.ProjectProposals.ToListAsync());
        }

        // GET: ProjectProposals/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectProposal projectProposal = await db.ProjectProposals.FindAsync(id);
            if (projectProposal == null)
            {
                return HttpNotFound();
            }
            return View(projectProposal);
        }

        // GET: ProjectProposals/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProjectProposals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Body,Status,EndDate,CreatedAt,UpdatedAt")] ProjectProposal projectProposal)
        {
            if (ModelState.IsValid)
            {
                projectProposal.Id = Guid.NewGuid();
                db.ProjectProposals.Add(projectProposal);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(projectProposal);
        }

        // GET: ProjectProposals/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectProposal projectProposal = await db.ProjectProposals.FindAsync(id);
            if (projectProposal == null)
            {
                return HttpNotFound();
            }
            return View(projectProposal);
        }

        // POST: ProjectProposals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Body,Status,EndDate,CreatedAt,UpdatedAt")] ProjectProposal projectProposal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(projectProposal).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(projectProposal);
        }

        // GET: ProjectProposals/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectProposal projectProposal = await db.ProjectProposals.FindAsync(id);
            if (projectProposal == null)
            {
                return HttpNotFound();
            }
            return View(projectProposal);
        }

        // POST: ProjectProposals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            ProjectProposal projectProposal = await db.ProjectProposals.FindAsync(id);
            db.ProjectProposals.Remove(projectProposal);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
