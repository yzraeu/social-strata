﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SocialStrata.Models;

namespace SocialStrata.Controllers
{
	public class UsersController : Controller
	{
		private ModelContext db = new ModelContext();

		// GET: Users
		public async Task<ActionResult> Index()
		{
			return View(await db.Users.ToListAsync());
		}

		// GET: Users/Details/5
		public async Task<ActionResult> Details(Guid? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			User user = await db.Users.FindAsync(id);
			if (user == null)
			{
				return HttpNotFound();
			}
			return View(user);
		}

		// GET: Users/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: Users/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Create([Bind(Include = "Id,FirstName,LastName,Email,PasswordHash,DateOfBirth,Photo,Biography,Interests,UserType,CreatedAt,UpdatedAt")] User user)
		{
			if (!ModelState.IsValid) return View(user);

			user.Id = Guid.NewGuid();

			if (Request.Files.Count > 0)
			{
				var file = Request.Files[0];

				if (file != null && file.ContentLength > 0)
				{
					var fileExtension = Path.GetExtension(file.FileName);
					var filename = "User_" + user.Id + fileExtension;
					var path = Path.Combine(Server.MapPath("~/Images/"), filename);
					file.SaveAs(path);

					user.Photo = "/Images/" + filename;
				}
			}


			db.Users.Add(user);
			await db.SaveChangesAsync();

			return RedirectToAction("Index");
		}

		// GET: Users/Edit/5
		public async Task<ActionResult> Edit(Guid? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			User user = await db.Users.FindAsync(id);
			if (user == null)
			{
				return HttpNotFound();
			}
			return View(user);
		}

		// POST: Users/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Edit([Bind(Include = "Id,FirstName,LastName,Email,PasswordHash,DateOfBirth,Photo,Biography,Interests,UserType,CreatedAt,UpdatedAt")] User user)
		{
			if (!ModelState.IsValid) return View(user);

			if (Request.Files.Count > 0)
			{
				var file = Request.Files[0];

				if (file != null && file.ContentLength > 0)
				{
					var fileExtension = Path.GetExtension(file.FileName);
					var filename = "User_" + user.Id + fileExtension;
					var path = Path.Combine(Server.MapPath("~/Images/"), filename);
					if (System.IO.File.Exists(path)) System.IO.File.Delete(path);
					file.SaveAs(path);

					user.Photo = "/Images/" + filename;
				}
			}

			db.Entry(user).State = EntityState.Modified;
			await db.SaveChangesAsync();
			return RedirectToAction("Index");
		}


		// GET: Users/Delete/5
		public async Task<ActionResult> Delete(Guid? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			User user = await db.Users.FindAsync(id);
			if (user == null)
			{
				return HttpNotFound();
			}
			db.Users.Remove(user);
			await db.SaveChangesAsync();
			return RedirectToAction("Index");
		}
		
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
