namespace SocialStrata.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Number = c.Int(nullable: false),
                        Street = c.String(nullable: false, maxLength: 300),
                        City = c.String(nullable: false, maxLength: 300),
                        ZipCode = c.String(nullable: false, maxLength: 30),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        State_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.State_Id, cascadeDelete: true)
                .Index(t => t.State_Id);
            
            CreateTable(
                "dbo.State",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 300),
                        Code = c.String(nullable: false, maxLength: 2),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Country_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Country", t => t.Country_Id, cascadeDelete: true)
                .Index(t => t.Country_Id);
            
            CreateTable(
                "dbo.Country",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        CountryCode = c.String(nullable: false, maxLength: 2),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Amenity",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Photo = c.String(),
                        IsSchedulable = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Building_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Building", t => t.Building_Id)
                .Index(t => t.Building_Id);
            
            CreateTable(
                "dbo.AmenitySchedule",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Amenity_Id = c.Guid(nullable: false),
                        Building_Id = c.Guid(nullable: false),
                        User_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Amenity", t => t.Amenity_Id, cascadeDelete: true)
                .ForeignKey("dbo.Building", t => t.Building_Id, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Amenity_Id)
                .Index(t => t.Building_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Building",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 300),
                        DateOfConstruction = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Address_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Address", t => t.Address_Id)
                .Index(t => t.Address_Id);
            
            CreateTable(
                "dbo.Tower",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Floors = c.Int(nullable: false),
                        ApartatmentsPerFloor = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Building_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Building", t => t.Building_Id, cascadeDelete: true)
                .Index(t => t.Building_Id);
            
            CreateTable(
                "dbo.Apartment",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Number = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Tower_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tower", t => t.Tower_Id, cascadeDelete: true)
                .Index(t => t.Tower_Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        PasswordHash = c.String(nullable: false),
                        DateOfBirth = c.DateTime(nullable: false),
                        Photo = c.String(),
                        Biography = c.String(),
                        Interests = c.String(),
                        UserType = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Apartment_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartment", t => t.Apartment_Id)
                .Index(t => t.Apartment_Id);
            
            CreateTable(
                "dbo.Telephone",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Number = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.LostAndFound",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 300),
                        Photo = c.String(),
                        Status = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Building_Id = c.Guid(nullable: false),
                        User_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Building", t => t.Building_Id, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Building_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.MaintenanceRequest",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(nullable: false),
                        Body = c.String(nullable: false),
                        Status = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Building_Id = c.Guid(nullable: false),
                        User_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Building", t => t.Building_Id, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Building_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.NoticeBoard",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(nullable: false),
                        Body = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Building_Id = c.Guid(nullable: false),
                        User_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Building", t => t.Building_Id, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Building_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Notification",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IsRead = c.Boolean(nullable: false),
                        Text = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        User_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.ProjectProposal",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(nullable: false),
                        Body = c.String(nullable: false),
                        Status = c.Int(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Building_Id = c.Guid(nullable: false),
                        User_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Building", t => t.Building_Id, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Building_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.ProjectProposalVote",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Agree = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Building_Id = c.Guid(nullable: false),
                        User_Id = c.Guid(nullable: false),
                        ProjectProposal_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Building", t => t.Building_Id, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.ProjectProposal", t => t.ProjectProposal_Id)
                .Index(t => t.Building_Id)
                .Index(t => t.User_Id)
                .Index(t => t.ProjectProposal_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectProposalVote", "ProjectProposal_Id", "dbo.ProjectProposal");
            DropForeignKey("dbo.ProjectProposalVote", "User_Id", "dbo.User");
            DropForeignKey("dbo.ProjectProposalVote", "Building_Id", "dbo.Building");
            DropForeignKey("dbo.ProjectProposal", "User_Id", "dbo.User");
            DropForeignKey("dbo.ProjectProposal", "Building_Id", "dbo.Building");
            DropForeignKey("dbo.Notification", "User_Id", "dbo.User");
            DropForeignKey("dbo.NoticeBoard", "User_Id", "dbo.User");
            DropForeignKey("dbo.NoticeBoard", "Building_Id", "dbo.Building");
            DropForeignKey("dbo.MaintenanceRequest", "User_Id", "dbo.User");
            DropForeignKey("dbo.MaintenanceRequest", "Building_Id", "dbo.Building");
            DropForeignKey("dbo.LostAndFound", "User_Id", "dbo.User");
            DropForeignKey("dbo.LostAndFound", "Building_Id", "dbo.Building");
            DropForeignKey("dbo.AmenitySchedule", "User_Id", "dbo.User");
            DropForeignKey("dbo.Telephone", "User_Id", "dbo.User");
            DropForeignKey("dbo.User", "Apartment_Id", "dbo.Apartment");
            DropForeignKey("dbo.AmenitySchedule", "Building_Id", "dbo.Building");
            DropForeignKey("dbo.Tower", "Building_Id", "dbo.Building");
            DropForeignKey("dbo.Apartment", "Tower_Id", "dbo.Tower");
            DropForeignKey("dbo.Amenity", "Building_Id", "dbo.Building");
            DropForeignKey("dbo.Building", "Address_Id", "dbo.Address");
            DropForeignKey("dbo.AmenitySchedule", "Amenity_Id", "dbo.Amenity");
            DropForeignKey("dbo.Address", "State_Id", "dbo.State");
            DropForeignKey("dbo.State", "Country_Id", "dbo.Country");
            DropIndex("dbo.ProjectProposalVote", new[] { "ProjectProposal_Id" });
            DropIndex("dbo.ProjectProposalVote", new[] { "User_Id" });
            DropIndex("dbo.ProjectProposalVote", new[] { "Building_Id" });
            DropIndex("dbo.ProjectProposal", new[] { "User_Id" });
            DropIndex("dbo.ProjectProposal", new[] { "Building_Id" });
            DropIndex("dbo.Notification", new[] { "User_Id" });
            DropIndex("dbo.NoticeBoard", new[] { "User_Id" });
            DropIndex("dbo.NoticeBoard", new[] { "Building_Id" });
            DropIndex("dbo.MaintenanceRequest", new[] { "User_Id" });
            DropIndex("dbo.MaintenanceRequest", new[] { "Building_Id" });
            DropIndex("dbo.LostAndFound", new[] { "User_Id" });
            DropIndex("dbo.LostAndFound", new[] { "Building_Id" });
            DropIndex("dbo.Telephone", new[] { "User_Id" });
            DropIndex("dbo.User", new[] { "Apartment_Id" });
            DropIndex("dbo.Apartment", new[] { "Tower_Id" });
            DropIndex("dbo.Tower", new[] { "Building_Id" });
            DropIndex("dbo.Building", new[] { "Address_Id" });
            DropIndex("dbo.AmenitySchedule", new[] { "User_Id" });
            DropIndex("dbo.AmenitySchedule", new[] { "Building_Id" });
            DropIndex("dbo.AmenitySchedule", new[] { "Amenity_Id" });
            DropIndex("dbo.Amenity", new[] { "Building_Id" });
            DropIndex("dbo.State", new[] { "Country_Id" });
            DropIndex("dbo.Address", new[] { "State_Id" });
            DropTable("dbo.ProjectProposalVote");
            DropTable("dbo.ProjectProposal");
            DropTable("dbo.Notification");
            DropTable("dbo.NoticeBoard");
            DropTable("dbo.MaintenanceRequest");
            DropTable("dbo.LostAndFound");
            DropTable("dbo.Telephone");
            DropTable("dbo.User");
            DropTable("dbo.Apartment");
            DropTable("dbo.Tower");
            DropTable("dbo.Building");
            DropTable("dbo.AmenitySchedule");
            DropTable("dbo.Amenity");
            DropTable("dbo.Country");
            DropTable("dbo.State");
            DropTable("dbo.Address");
        }
    }
}
