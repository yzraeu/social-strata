namespace SocialStrata.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DescriptionInLostAndFound : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LostAndFound", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LostAndFound", "Description");
        }
    }
}
