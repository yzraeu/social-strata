// <auto-generated />
namespace SocialStrata.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class UserAndBuildingId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UserAndBuildingId));
        
        string IMigrationMetadata.Id
        {
            get { return "201605220923171_UserAndBuildingId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
