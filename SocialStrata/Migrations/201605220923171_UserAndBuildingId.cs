namespace SocialStrata.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserAndBuildingId : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.AmenitySchedule", name: "Building_Id", newName: "BuildingId");
            RenameColumn(table: "dbo.AmenitySchedule", name: "User_Id", newName: "UserId");
            RenameColumn(table: "dbo.LostAndFound", name: "Building_Id", newName: "BuildingId");
            RenameColumn(table: "dbo.LostAndFound", name: "User_Id", newName: "UserId");
            RenameColumn(table: "dbo.MaintenanceRequest", name: "Building_Id", newName: "BuildingId");
            RenameColumn(table: "dbo.MaintenanceRequest", name: "User_Id", newName: "UserId");
            RenameColumn(table: "dbo.NoticeBoard", name: "Building_Id", newName: "BuildingId");
            RenameColumn(table: "dbo.NoticeBoard", name: "User_Id", newName: "UserId");
            RenameColumn(table: "dbo.ProjectProposal", name: "Building_Id", newName: "BuildingId");
            RenameColumn(table: "dbo.ProjectProposal", name: "User_Id", newName: "UserId");
            RenameColumn(table: "dbo.ProjectProposalVote", name: "Building_Id", newName: "BuildingId");
            RenameColumn(table: "dbo.ProjectProposalVote", name: "User_Id", newName: "UserId");
            RenameIndex(table: "dbo.AmenitySchedule", name: "IX_Building_Id", newName: "IX_BuildingId");
            RenameIndex(table: "dbo.AmenitySchedule", name: "IX_User_Id", newName: "IX_UserId");
            RenameIndex(table: "dbo.LostAndFound", name: "IX_Building_Id", newName: "IX_BuildingId");
            RenameIndex(table: "dbo.LostAndFound", name: "IX_User_Id", newName: "IX_UserId");
            RenameIndex(table: "dbo.MaintenanceRequest", name: "IX_Building_Id", newName: "IX_BuildingId");
            RenameIndex(table: "dbo.MaintenanceRequest", name: "IX_User_Id", newName: "IX_UserId");
            RenameIndex(table: "dbo.NoticeBoard", name: "IX_Building_Id", newName: "IX_BuildingId");
            RenameIndex(table: "dbo.NoticeBoard", name: "IX_User_Id", newName: "IX_UserId");
            RenameIndex(table: "dbo.ProjectProposal", name: "IX_Building_Id", newName: "IX_BuildingId");
            RenameIndex(table: "dbo.ProjectProposal", name: "IX_User_Id", newName: "IX_UserId");
            RenameIndex(table: "dbo.ProjectProposalVote", name: "IX_Building_Id", newName: "IX_BuildingId");
            RenameIndex(table: "dbo.ProjectProposalVote", name: "IX_User_Id", newName: "IX_UserId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ProjectProposalVote", name: "IX_UserId", newName: "IX_User_Id");
            RenameIndex(table: "dbo.ProjectProposalVote", name: "IX_BuildingId", newName: "IX_Building_Id");
            RenameIndex(table: "dbo.ProjectProposal", name: "IX_UserId", newName: "IX_User_Id");
            RenameIndex(table: "dbo.ProjectProposal", name: "IX_BuildingId", newName: "IX_Building_Id");
            RenameIndex(table: "dbo.NoticeBoard", name: "IX_UserId", newName: "IX_User_Id");
            RenameIndex(table: "dbo.NoticeBoard", name: "IX_BuildingId", newName: "IX_Building_Id");
            RenameIndex(table: "dbo.MaintenanceRequest", name: "IX_UserId", newName: "IX_User_Id");
            RenameIndex(table: "dbo.MaintenanceRequest", name: "IX_BuildingId", newName: "IX_Building_Id");
            RenameIndex(table: "dbo.LostAndFound", name: "IX_UserId", newName: "IX_User_Id");
            RenameIndex(table: "dbo.LostAndFound", name: "IX_BuildingId", newName: "IX_Building_Id");
            RenameIndex(table: "dbo.AmenitySchedule", name: "IX_UserId", newName: "IX_User_Id");
            RenameIndex(table: "dbo.AmenitySchedule", name: "IX_BuildingId", newName: "IX_Building_Id");
            RenameColumn(table: "dbo.ProjectProposalVote", name: "UserId", newName: "User_Id");
            RenameColumn(table: "dbo.ProjectProposalVote", name: "BuildingId", newName: "Building_Id");
            RenameColumn(table: "dbo.ProjectProposal", name: "UserId", newName: "User_Id");
            RenameColumn(table: "dbo.ProjectProposal", name: "BuildingId", newName: "Building_Id");
            RenameColumn(table: "dbo.NoticeBoard", name: "UserId", newName: "User_Id");
            RenameColumn(table: "dbo.NoticeBoard", name: "BuildingId", newName: "Building_Id");
            RenameColumn(table: "dbo.MaintenanceRequest", name: "UserId", newName: "User_Id");
            RenameColumn(table: "dbo.MaintenanceRequest", name: "BuildingId", newName: "Building_Id");
            RenameColumn(table: "dbo.LostAndFound", name: "UserId", newName: "User_Id");
            RenameColumn(table: "dbo.LostAndFound", name: "BuildingId", newName: "Building_Id");
            RenameColumn(table: "dbo.AmenitySchedule", name: "UserId", newName: "User_Id");
            RenameColumn(table: "dbo.AmenitySchedule", name: "BuildingId", newName: "Building_Id");
        }
    }
}
