using System.Collections.Generic;
using System.Data.Entity.Migrations.Design;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.SqlServer;

namespace SocialStrata.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SocialStrata.Models.ModelContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
			SetSqlGenerator("System.Data.SqlClient", new CustomSqlServerMigrationSqlGenerator());
		}

	    internal class CustomSqlServerMigrationSqlGenerator : SqlServerMigrationSqlGenerator
	    {
		    protected override void Generate(AddColumnOperation addColumnOperation)
		    {
			    SetCreatedUtcColumn(addColumnOperation.Column);

			    base.Generate(addColumnOperation);
		    }

		    protected override void Generate(CreateTableOperation createTableOperation)
		    {
			    SetCreatedUtcColumn(createTableOperation.Columns);

			    base.Generate(createTableOperation);
		    }

		    private static void SetCreatedUtcColumn(IEnumerable<ColumnModel> columns)
		    {
			    foreach (var columnModel in columns)
			    {
				    SetCreatedUtcColumn(columnModel);
			    }
		    }

		    private static void SetCreatedUtcColumn(PropertyModel column)
		    {
			    if (column.Name == "CreatedAt" || column.Name == "UpdatedAt")
			    {
				    column.DefaultValueSql = "GETDATE()";
			    }
		    }
	    }

	    protected override void Seed(SocialStrata.Models.ModelContext context)
        {
          
        }
    }
	
}
