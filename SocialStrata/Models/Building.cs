﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;

namespace SocialStrata.Models
{
	public class Building : BaseEntity
	{
		[Required, MaxLength(300)]
		public string Name { get; set; }
		[Required]
		public Guid AddressId { get; set; }
		public Address Address { get; set; }
		[Required]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime DateOfConstruction { get; set; }
		public ICollection<Amenity> Amenities { get; set; }
		public ICollection<Tower> Towers { get; set; }
	}

	public class Address : BaseEntity
	{
		[Required]
		public int Number { get; set; }
		[Required, MaxLength(300)]
		public string Street { get; set; }
		[Required, MaxLength(300)]
		public string City { get; set; }
		[Required]
		public Guid StateId { get; set; }
		public State State { get; set; }
		[Required, MaxLength(30)]
		public string ZipCode { get; set; }

		public string StreetLine => Number + " " + Street;
	}

	public class State : BaseEntity
	{
		[Required, MaxLength(300)]
		public string Name { get; set; }
		[Required, MaxLength(2)]
		public string Code { get; set; }
		[Required]
		public Guid CountryId { get; set; }
		public Country Country { get; set; }
	}

	public class Country : BaseEntity
	{
		[Required]
		public string Name { get; set; }
		[Required, MaxLength(2)]
		public string CountryCode { get; set; }
	}

	public class Tower : BaseEntity
	{
		[Required, MaxLength(100)]
		public string Name { get; set; }
		[Required]
		public int Floors { get; set; }
		[Required]
		public int ApartmentsPerFloor { get; set; }
		public virtual ICollection<Apartment> Apartments { get; set; }
		[Required]
		public Guid BuildingId { get; set; }
		public virtual Building Building { get; set; }
	}

	public class Apartment : BaseEntity
	{
		[Required]
		public string Number { get; set; }
		[Required]
		public Guid TowerId { get; set; }
		public virtual Tower Tower { get; set; }
	}

	public class Amenity : BaseEntity
	{
		[Required]
		public string Name { get; set; }
		public string Photo { get; set; }
		public bool IsSchedulable { get; set; }
	}
}