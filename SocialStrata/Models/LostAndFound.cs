﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SocialStrata.Models
{
	public class LostAndFound : BaseBuildingEntity
	{
		[Required, MaxLength(300)]
		public string Name { get; set; }
		public string Description { get; set; }
		public string Photo { get; set; }
		[Range(1, 3)]
		public LostAndFoundStatus Status { get; set; }
	}

	public enum LostAndFoundStatus
	{
		NotSet = 0,
		Lost,
		Found,
		Retrieved
	}
}