﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SocialStrata.Models
{
	public class MaintenanceRequest : BaseBuildingEntity
	{
		[Required]
		public string Title { get; set; }
		[Required]
		public string Body { get; set; }
		public MaintenanceRequestStatus Status { get; set; }
	}

	public enum MaintenanceRequestStatus
	{
		NotSet = 0,
		Open,
		WaitingReply,
		Closed
	}
}