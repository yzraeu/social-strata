using System.Data.Entity.ModelConfiguration.Conventions;

namespace SocialStrata.Models
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class ModelContext : DbContext
	{
		public ModelContext()
			: base("name=Database")
		{
		}

		public DbSet<User> Users { get; set; }
		//public DbSet<UserType> UserType { get; set; }
		public DbSet<Telephone> Telephones { get; set; }
		//public DbSet<ContactType> ContactType { get; set; }
		public DbSet<Notification> Notifications { get; set; }
		public DbSet<Building> Buildings { get; set; }
		public DbSet<Address> Addresses { get; set; }
		public DbSet<State> States { get; set; }
		public DbSet<Country> Countries { get; set; }
		public DbSet<Tower> Towers { get; set; }
		public DbSet<Apartment> Apartments { get; set; }
		public DbSet<Amenity> Amenities { get; set; }
		public DbSet<AmenitySchedule> AmenitySchedules { get; set; }
		public DbSet<LostAndFound> LostAndFound { get; set; }
		//public DbSet<LostAndFoundStatus> LostAndFoundStatus { get; set; }
		public DbSet<MaintenanceRequest> MaintenanceRequests { get; set; }
		//public DbSet<MaintenanceRequestStatus> MaintenanceRequestStatus { get; set; }
		public DbSet<NoticeBoard> NoticeBoard { get; set; }
		public DbSet<ProjectProposal> ProjectProposals { get; set; }
		public DbSet<ProjectProposalVote> ProjectProposalVotes { get; set; }
		
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
		}
	}
}
