﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SocialStrata.Models
{
	public class NoticeBoard : BaseBuildingEntity
	{
		[Required]
		public string Title { get; set; }
		[Required]
		public string Body { get; set; }
	}
}