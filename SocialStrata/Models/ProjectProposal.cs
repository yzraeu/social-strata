﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SocialStrata.Models
{
	public class ProjectProposal : BaseBuildingEntity
	{
		[Required]
		public string Title { get; set; }
		[Required]
		public string Body { get; set; }
		public ProjectProposalStatus Status { get; set; }
		public DateTime EndDate { get; set; }
		public virtual ICollection<ProjectProposalVote> Votes { get; set; }
	}

	public enum ProjectProposalStatus
	{
		NotSet = 0,
		Open,
		Accepted,
		Denied
	}

	public class ProjectProposalVote : BaseBuildingEntity
	{
		public bool Agree { get; set; }
	}
}