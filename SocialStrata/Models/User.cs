﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Net.Mail;

namespace SocialStrata.Models
{
	public class User : BaseEntity
	{
		[Required]
		public string FirstName { get; set; }
		[Required]
		public string LastName { get; set; }
		[Required]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }
		[Required]
		[DataType(DataType.Password)]
		public string PasswordHash { get; set; }
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime DateOfBirth { get; set; }
		public string Photo { get; set; }
		public string Biography { get; set; }
		public string Interests { get; set; }
		public ICollection<Telephone> Telephones { get; set; }
		public UserType UserType { get; set; }
		public Guid ApartmentId { get; set; }
		public virtual Apartment Apartment { get; set; }
	}

	public enum UserType
	{
		NotSet = 0,
		Tenant,
		Landlord,
		Admin
	}

	public class Telephone : BaseEntity
	{
		public ContactType Type { get; set; }
		public string Number { get; set; }
	}

	public enum ContactType
	{
		NotSet = 0,
		Main,
		Mobile,
		Work,
		Home
	}

	public class Notification : BaseEntity
	{
		public bool IsRead { get; set; }
		[Required]
		public string Text { get; set; }
		[Required]
		public Guid UserId { get; set; }
		public virtual User User { get; set; }
	}
}