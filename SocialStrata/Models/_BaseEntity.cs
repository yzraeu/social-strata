﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SocialStrata.Models
{
	public class BaseEntity
	{
		[Key]
		public Guid Id { get; set; }
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public DateTime CreatedAt { get; protected set; }
		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime UpdatedAt { get; protected set; }
	}

	public class BaseBuildingEntity : BaseEntity
	{
		[Required]
		public Guid BuildingId { get; set; }
		public virtual Building Building { get; set; }
		[Required]
		public Guid UserId { get; set; }
		public virtual User User { get; set; }
	}
}