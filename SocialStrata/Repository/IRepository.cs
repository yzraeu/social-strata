﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SocialStrata.Models;

namespace SocialStrata.Repository
{
	public interface IRepository<TEntity> where TEntity : class
	{
		List<TEntity> GetAll();
		List<TEntity> Get(Func<TEntity, bool> where);
		void Insert(TEntity entity);
		void Insert(IEnumerable<TEntity> entities);
		void Remove(TEntity entity);
		void Remove(IEnumerable<TEntity> entities);
	}
}
