﻿var towerCount = 0;
var addTower = function () {
	var container = $("#tower-container");
	var template = $("#tower-template").html().replace(/{{c}}/g, towerCount);
	container.append(template);
	towerCount++;
};

var removeTower = function (button) {
	console.log($(button).parent().remove());
	towerCount--;
};

var amenityCount = 0;
var addAmenity = function () {
	var container = $("#amenity-container");
	var template = $("#amenity-template").html().replace(/{{c}}/g, amenityCount);
	container.append(template);
	amenityCount++;
};

var removeAmenity = function (button) {
	console.log($(button).parent().remove());
	amenityCount--;
};